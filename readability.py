from tkinter import *
from tkinter import filedialog
import textract
from words import word_count

# pyinstaller -w -F -i <icon_file> <python_file>

ftypes = [
    ('Article Files', '*.doc;*.docx;*.pdf'),
    ('All Files', '*.*')
]

def open_file():
    file_name = filedialog.askopenfilename(initialdir="~/",
                                           title="Select file",
                                           filetypes=ftypes)

    text = textract.process(file_name).decode('utf-8')

    wc = word_count(text)
    word2,word3,word4,word5,word6,wordt = wc.get_numbers()

    w2.config(text='2 heceli kelime sayisi: ' + str(word2))
    w3.config(text='3 heceli kelime sayisi: ' + str(word3))
    w4.config(text='4 heceli kelime sayisi: ' + str(word4))
    w5.config(text='5 heceli kelime sayisi: ' + str(word5))
    w6.config(text='6 heceli kelime sayisi: ' + str(word6))
    wt.config(text='Toplam hece sayisi: ' + str(wordt))


if __name__ == "__main__":
    root = Tk()
    root.geometry("750x400")
    root.configure(background='white')
    root.title("Readability calculator")

    fopen = Button(root,
                   text="Open File",
                   fg='black',
                   width=106,
                   height=3,
                   command=open_file)
    fopen.grid(row=0)

    w2 = Label(root,
                   text="2 heceli kelime sayisi: 0",
                   fg='black')

    w2.grid(row=1)

    w3 = Label(root,
               text="3 heceli kelime sayisi: 0",
               fg='black')

    w3.grid(row=2)

    w4 = Label(root,
                   text="4 heceli kelime sayisi: 0",
                   fg='black')

    w4.grid(row=3)

    w5 = Label(root,
               text="5 heceli kelime sayisi: 0",
               fg='black')

    w5.grid(row=4)

    w6 = Label(root,
               text="6 heceli kelime sayisi: 0",
               fg='black')

    w6.grid(row=5)

    wt = Label(root,
               text="Toplam hece sayisi: 0",
               fg='black')

    wt.grid(row=6)

    root.mainloop()
